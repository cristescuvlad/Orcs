package com.example.vlad.orcs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.media.MediaPlayer;

import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

public class MainView extends SurfaceView implements Runnable {

    public MainView(Context context) {
        super(context);
        this.context = context;

        ourHolder = getHolder();
        paint = new Paint();
        playing = true;
        rnd = new Random();
        evilCharacters = new Vector<Character>();
        goodCharacters = new Vector<Character>();
        spawnCount = new int[Constants.characterTypesCount];
        millisSinceStart = System.currentTimeMillis();
    }

    @Override
    public void run() {
        while (playing) {
            update();
            draw();
        }
    }

    public void update() {
        millisSinceStart = System.currentTimeMillis() - millisSinceStart;

        //remove dead characters
        for (int i = 0; i < this.evilCharacters.size(); i++) {
            Character chr = evilCharacters.elementAt(i);
            if (chr.isDead) {
                spawnCount[Constants.CharactersIDMap.get(chr.getClass().getSimpleName())]--;
                evilCharacters.remove(i);
            }
        }
        for (int i = 0; i < this.goodCharacters.size(); i++) {
            Character chr = goodCharacters.elementAt(i);
            if (chr.isDead) {
                spawnCount[Constants.CharactersIDMap.get(chr.getClass().getSimpleName())]--;
                goodCharacters.remove(i);
            }
        }

        //update characters
        for (int i = 0; i < this.evilCharacters.size(); i++) {
            Character character = this.evilCharacters.elementAt(i);
            character.setMovementVector(character.getVectorToClosestEnemy(goodCharacters));
            Character closestEnemy = character.findClosestEnemy(goodCharacters);
            if (closestEnemy != null && character.isInAttackRange(closestEnemy.getPosition())) {
                character.attack(closestEnemy);  //should be time based
            } else {
                character.move(); //should be time based
            }
        }

        for (int i = 0; i < this.goodCharacters.size(); i++) {
            Character character = this.goodCharacters.elementAt(i);
            character.setMovementVector(character.getVectorToClosestEnemy(evilCharacters));
            Character closestEnemy = character.findClosestEnemy(evilCharacters);
            if (closestEnemy != null && character.isInAttackRange(closestEnemy.getPosition())) {
                character.attack(closestEnemy);
            } else {
                character.move(); //should be time based
            }
        }
    }

    public void draw() {
        if (ourHolder.getSurface().isValid()) {
            canvas = ourHolder.lockCanvas();

            paint.setColor(Color.GREEN);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPaint(paint);

            for (int i = 0; i < this.evilCharacters.size(); i++) {
                Character character = evilCharacters.elementAt(i);
                canvas.drawBitmap(character.getSpriteBitmap(), character.getPosition().x, character.getPosition().y, paint);
                paint.setColor(Color.BLACK);
                canvas.drawText(character.getCharacterType() + " " + Integer.toString(character.getHealth()), character.getPosition().x, character.getPosition().y, paint);
            }

            for (int i = 0; i < this.goodCharacters.size(); i++) {
                Character character = goodCharacters.elementAt(i);
                canvas.drawBitmap(character.getSpriteBitmap(), character.getPosition().x, character.getPosition().y, paint);
                paint.setColor(Color.BLACK);
                canvas.drawText(character.getCharacterType() + " " + Integer.toString(character.getHealth()), character.getPosition().x, character.getPosition().y, paint);
            }

            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void pause() {
        playing = false;
        if(soundtrackStarted){
            soundtrackPlayer.pause();
        }
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
        if(soundtrackStarted){
            soundtrackPlayer.start();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        if(x < 5 || y < 5 || x > this.getWidth()-100 || y > this.getHeight()-100){
            return false;
        }

        if(!soundtrackStarted){
            soundtrackStarted = true;
            soundtrackPlayer = MediaPlayer.create(context, R.raw.soundtrack);
            soundtrackPlayer.setLooping(true);
            soundtrackPlayer.start();
        }

        int randomNr = 0;
        do{ //make sure maxAllowedOfType[] has a plausible value and distribution
            randomNr = rnd.nextInt(Constants.goodCharacters.size()+Constants.evilCharacters.size());
            if(x < this.getWidth()/2) {
                randomNr = (randomNr % Constants.goodCharacters.size()) + Constants.evilCharacters.size();
            }
            else{
                randomNr = randomNr % Constants.evilCharacters.size();
            }
        }
        while(Constants.maxAllowedOfType[randomNr] >= 0 && spawnCount[randomNr] >= Constants.maxAllowedOfType[randomNr]);
        spawnCount[randomNr]++;

        Character newCharacter = Character.getCharacterOfType(randomNr,x,y,this.getResources());
        if(newCharacter == null){
            return false;
        }

        if(Constants.evilCharacters.contains(newCharacter.getClass().getSimpleName())){
            newCharacter.setMovementVector(newCharacter.getVectorToClosestEnemy(goodCharacters));
            evilCharacters.add(newCharacter);
        }
        else{
            newCharacter.setMovementVector(newCharacter.getVectorToClosestEnemy(evilCharacters));
            goodCharacters.add(newCharacter);
        }

        MediaPlayer mp = MediaPlayer.create(context, Constants.CharactersSoundMap.get(newCharacter.getClass().getSimpleName()));
        if(mp != null){
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
                public void onCompletion(MediaPlayer player) {
                    player.release();
                }
            });
        }

        return false;
    }

    //member vars

    private Context context = null;
    private long millisSinceStart = 0;
    private Thread gameThread = null;
    private SurfaceHolder ourHolder = null;
    private volatile boolean playing = false;
    private Canvas canvas = null;
    private Paint paint = null;
    private Vector<Character> goodCharacters = null;
    private Vector<Character> evilCharacters = null;
    private Random rnd = null;
    private boolean soundtrackStarted = false;
    private MediaPlayer soundtrackPlayer = null;
    private int[] spawnCount = null;
}