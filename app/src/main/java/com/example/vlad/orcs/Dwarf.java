package com.example.vlad.orcs;

import android.content.res.Resources;

/**
 * Created by Vlad on 17.04.2017.
 */

public class Dwarf extends Character{

    public Dwarf(int x, int y, float scale, String name, Resources resources) {
        super(x,y,scale,name,resources,R.drawable.dwarf);
    }
}
