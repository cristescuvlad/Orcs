package com.example.vlad.orcs;

import android.content.res.Resources;

public class Orc extends Character{

    public Orc(int x, int y, float scale, String name, Resources resources) {
        super(x,y,scale,name,resources,R.drawable.orc);
    }
}
