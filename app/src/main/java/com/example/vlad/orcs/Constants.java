package com.example.vlad.orcs;

import java.util.ArrayList;
import java.util.HashMap;

public class Constants {
    public static final ArrayList<String> evilCharacters = new ArrayList<String>(){{
        add("Troll");
        add("Orc");
        add("Goblin");
    }};
    public static final ArrayList<String> goodCharacters = new ArrayList<String>(){{
        add("Human");
        add("Elf");
        add("Dwarf");
        add("Ent");
    }};

    public static final HashMap<String, Integer> CharactersIDMap = new HashMap<String, Integer>() {{
        put("Troll", 0);
        put("Orc", 1);
        put("Goblin", 2);
        put("Human", 3);
        put("Elf", 4);
        put("Dwarf", 5);
        put("Ent", 6);
    }};

    public static final HashMap<String, Integer> CharactersSoundMap = new HashMap<String, Integer>() {{
        put("Troll", R.raw.troll_sound);
        put("Orc", R.raw.orc_sound);
        put("Goblin", R.raw.goblin_sound);
        put("Human", R.raw.human_sound);
        put("Elf", R.raw.elf_sound);
        put("Dwarf", R.raw.dwarf_sound);
        put("Ent", R.raw.ent_sound);
    }};

    public static final int characterTypesCount = evilCharacters.size() + goodCharacters.size();

    public static final int[] maxAllowedOfType = {4,-1,-1,-1,-1,-1,4};
}
