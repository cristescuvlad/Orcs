package com.example.vlad.orcs;

import android.content.res.Resources;

public class Ent extends Character{

    public Ent(int x, int y, float scale, String name, Resources resources) {
        super(x,y,scale,name,resources,R.drawable.ent);
    }
}
