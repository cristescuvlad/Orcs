package com.example.vlad.orcs;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

import java.util.Vector;

public abstract class Character {

    public Character(int x, int y, float scale, String characterType, Resources resources, int spriteId){

        Bitmap b = BitmapFactory.decodeResource(resources, spriteId);
        this.sprite = Bitmap.createScaledBitmap(b,(int)(scale*b.getWidth()),(int)(scale*b.getHeight()),false);

        this.scale = scale;
        this.characterType = characterType;
        this.position = new Point(x-this.sprite.getWidth()/2,y-this.sprite.getHeight()/2);

        this.displacementVector = new Vector<Integer>(2);
    }

    //setters

    public void setMovementVector(Vector<Integer> vector){
        this.displacementVector = vector;
    }

    public void setSpeed(float speed){
        this.speed = speed;
    }

    public void setSprite(Resources resources, int spriteId){
        Bitmap b = BitmapFactory.decodeResource(resources, spriteId);
        this.sprite = Bitmap.createScaledBitmap(b,(int)(scale*b.getWidth()),(int)(scale*b.getHeight()),false);
    }

    public void setCharacterType(String characterType){
        this.characterType = characterType;
    }

    public void setHealth(int health){
        this.health = health;
    }

    public void setDamagePerHit(int damagePerHit){
        this.damagePerHit = damagePerHit;
    }

    public void setAttackRadius(int radius){
        this.attackRadius = radius;
    }

    public void setAttackSpeed(float attackSpeed) { this.attackSpeed = attackSpeed; }

    public void setStats(int health, float speed, int damagePerHit, int attackRadius, float attackSpeed){
        setHealth(health);
        setSpeed(speed);
        setDamagePerHit(damagePerHit);
        setAttackRadius(attackRadius);
        setAttackSpeed(attackSpeed);
    }


    //getters

    public Vector<Integer> getMovementVector() { return displacementVector; }

    public Point getPosition(){
        return this.position;
    }

    public String getCharacterType(){
        return this.characterType;
    }

    public int getDamagePerHit(){
        return this.damagePerHit;
    }

    public int getAttackRadius(){
        return this.attackRadius;
    }

    public int getHealth(){
        return this.health;
    }

    public float getAttackSpeed() { return this.attackSpeed; }

    public Bitmap getSpriteBitmap(){
        return this.sprite;
    }

    public static Character getCharacterOfType(int characterType, int x, int y, Resources resources){ //make sure you follow Constants.CharactersIDMap

        Character character = null;
        switch (characterType){
            case 0:
                character = new Troll(x, y, 0.4f, "Troll", resources);
                character.setStats(200,1,75,65,10.0f); //int health, float speed, int damagePerHit, int attackRadius, float attackSpeed (more like time units per attack)
                return character;
            case 1:
                character = new Orc(x, y, 0.2f, "Orc", resources);
                character.setStats(100,1.5f,20,60,5.0f);
                return character;
            case 2:
                character = new Goblin(x, y, 0.03f, "Goblin", resources);
                character.setStats(100,1.5f,45,60,4.0f);
                return character;
            case 3:
                character = new Human(x, y, 0.2f, "Human", resources);
                character.setStats(90,1.2f,25,60,5.0f);
                return character;
            case 4:
                character = new Elf(x, y, 0.07f, "Elf", resources);
                character.setStats(100,1.5f,3,200,3.0f);
                return character;
            case 5:
                character = new Dwarf(x, y, 0.065f, "Dwarf", resources);
                character.setStats(140,1.2f,65,60,7.0f);
                return character;
            case 6:
                character = new Ent(x, y, 0.8f, "Ent", resources);
                character.setStats(180,1,75,90,20.0f);
                return character;
            default:
                return null;
        }
    }
    //other methods

    public boolean isInAttackRange(Point point){
        double distance = Math.sqrt((this.position.x-point.x)*(this.position.x-point.x) + (this.position.y-point.y)*(this.position.y-point.y));
        if(distance <= this.getAttackRadius()){
            return true;
        }
        return false;
    }

    public void receiveDamage(int damageAmount){
        this.health -= damageAmount;
        if(this.health <= 0){
            this.isDead = true;
        }
    }

    public void move(){
        position.offset((int)(this.displacementVector.elementAt(0)*speed), (int)(this.displacementVector.elementAt(1)*speed));
    }

    public Character findClosestEnemy(Vector<Character> enemies){
        double min = -1;
        int closestEnemyIndex = -1;
        for(int i=0;i<enemies.size();i++){
            Point e_p = enemies.elementAt(i).getPosition();
            double distance = Math.sqrt((this.position.x-e_p.x)*(this.position.x-e_p.x) + (this.position.y-e_p.y)*(this.position.y-e_p.y));
            if(min < 0 || distance < min){
                min = distance;
                closestEnemyIndex = i;
            }
        }
        if(closestEnemyIndex >= 0){
            return enemies.elementAt(closestEnemyIndex);
        }
        return null;
    }

    public Vector<Integer> getVectorToClosestEnemy(Vector<Character> enemies){

        Character closestEnemy = this.findClosestEnemy(enemies);
        if(closestEnemy == null ){
            Vector<Integer> ret = new Vector<Integer>();
            ret.add(0);
            ret.add(0);
            return ret;
        }
        Point closestEnemyCoords = closestEnemy.getPosition();
        double distance = Math.sqrt((this.position.x-closestEnemyCoords.x)*(this.position.x-closestEnemyCoords.x) + (this.position.y-closestEnemyCoords.y)*(this.position.y-closestEnemyCoords.y));
        //return the vector to the closest enemy
        Vector<Integer> ret = new Vector<Integer>();
        ret.add((int)Math.ceil(speed*(closestEnemyCoords.x-position.x)/distance)); //normalize the vector and multiply by speed
        ret.add((int)Math.ceil(speed*(closestEnemyCoords.y-position.y)/distance));
        return ret;
    }

    public void attack (Character enemy){
        if(timeSinceLastHit >= attackSpeed){
            enemy.receiveDamage(this.getDamagePerHit());
            timeSinceLastHit = 0;
        }
        else{
            timeSinceLastHit++;
        }
    }

    //member vars

    protected Point position = null;
    protected float speed = 1;
    protected float scale = 1;
    protected String characterType = "";
    protected Bitmap sprite = null;
    protected Vector<Integer> displacementVector = null;

    protected int health = 0;
    protected int damagePerHit = 0;
    protected int attackRadius = 0;
    protected float attackSpeed = 0; //more like time units per attack
    protected int timeSinceLastHit = 0;
    public boolean isDead = false;
}
